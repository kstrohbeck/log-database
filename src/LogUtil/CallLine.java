package LogUtil;
import LogUtil.LogLine;
import java.util.List;
import java.util.Map;

public class CallLine extends LogLine {
    public CallLine(String address, String image, String function, String targetAddress, String targetImage, String targetFunction) {
        fields.put("name", "callline");
    	fields.put("address", address);
        fields.put("image", image);
        fields.put("function", function);
        fields.put("targetAddress", targetAddress);
        fields.put("targetImage", targetImage);
        fields.put("targetFunction", targetFunction);
    }
    public CallLine(List<String> fieldVals) {
        this(fieldVals.get(0), fieldVals.get(1), fieldVals.get(2), fieldVals.get(3), fieldVals.get(4), fieldVals.get(5));
    }
}