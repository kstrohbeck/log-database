package LogUtil;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

public class LogLine {
    protected Map<String, String> fields;

    public LogLine() {
        fields = new HashMap<String, String>();
    }

    public Map<String, String> getFields() {
        return Collections.unmodifiableMap(fields);
    }
}