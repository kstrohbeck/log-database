package LogUtil;
import java.util.Map;
import java.util.List;

public class ThreadStartLine extends LogLine {
    public ThreadStartLine(String time, String pid, String utid) {
    	fields.put("name", "threadstart");
        fields.put("time", time);
        fields.put("pid", pid);
        fields.put("utid", utid);
    }

    public ThreadStartLine(List<String> fieldVars) {
        this(fieldVars.get(0), fieldVars.get(1), fieldVars.get(2));
    }
}