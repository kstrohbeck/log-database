package LogUtil;
import java.util.Map;
import java.util.List;

public class MemoryWriteLine extends LogLine {
    public MemoryWriteLine(String address, String value) {
    	fields.put("name", "memwrite");
        fields.put("address", address);
        fields.put("value", value);
    }

    public MemoryWriteLine(List<String> fieldVars) {
        this(fieldVars.get(0), fieldVars.get(1));
    }
}