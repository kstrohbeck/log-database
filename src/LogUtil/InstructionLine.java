package LogUtil;
import LogUtil.LogLine;
import java.util.Map;
import java.util.List;

public class InstructionLine extends LogLine{
    public InstructionLine(String seq, String image, String function, String operation) {
        fields.put("name","instruction");
    	fields.put("seq", seq);
        fields.put("image", image);
        fields.put("function", function);
        fields.put("operation", operation);
    }

    public InstructionLine(List<String> fieldVars) {
        this(fieldVars.get(0), fieldVars.get(1), fieldVars.get(2), fieldVars.get(3));
    }
}