package LogUtil;
import java.util.List;
import java.util.Map;

public class MemoryReadLine extends LogLine {
    public MemoryReadLine(String value, String address) {
    	fields.put("name", "memread");
        fields.put("value", value);
        fields.put("address", address);
    }
    public MemoryReadLine(List<String> fieldVars) {
        this(fieldVars.get(0), fieldVars.get(1));
    }
}