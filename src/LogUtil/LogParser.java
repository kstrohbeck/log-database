package LogUtil;
import LogUtil.MemoryReadLine;
import LogUtil.MemoryWriteLine;
import LogUtil.ThreadStartLine;
import LogUtil.ThreadEndLine;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class LogParser {
    private BufferedReader reader;
    private String currentLine;
    private static Map<String, Pattern> allPatterns;
    private static List<String> patternOrder;
    static {
        Map<String, Pattern> tempPatterns = new HashMap<String, Pattern>();
        tempPatterns.put("InstructionLine", Pattern.compile("(0x(?i)[a-f(\\d+)]+)\\s*((?:/[\\w\\.\\-]*)+):(_\\S+)\\s*(.*)"));
        tempPatterns.put("CallLine", Pattern.compile(".*Call\\s*(0x(?i)[a-f(\\d+)]+)\\s*((?:/[\\w\\.\\-]*)+):(_\\S+)\\s*\\Q->\\E\\s*(0x(?i)[a-f(\\d+)]+)\\s*((?:/[\\w\\.\\-]*)+):(.+)"));
        tempPatterns.put("MemoryReadLine", Pattern.compile("\\s*Read\\s*(\\S+)\\s*=\\s*.*(0x(?i)[a-f(\\d+)]+)"));
        tempPatterns.put("MemoryWriteLine", Pattern.compile("\\s*Write.*?(0x(?i)[a-f(\\d+)]+)\\s*=\\s*(\\S+)"));
        tempPatterns.put("ThreadStartLine", Pattern.compile("New\\s*thread\\s*@\\s*time:\\s*(\\d+)\\s*w/Pin\\s*ID:\\s*(\\d+)\\s*&UserTID:\\s*(\\d+)"));
        tempPatterns.put("ThreadEndLine", Pattern.compile("Thread\\s*(\\d+)\\s*quit\\s*at\\s*(\\d+)\\s*.*"));
        allPatterns = Collections.unmodifiableMap(tempPatterns);
        patternOrder = new ArrayList<String>();
        patternOrder.add("CallLine");
        patternOrder.add("MemoryReadLine");
        patternOrder.add("MemoryWriteLine");
        patternOrder.add("InstructionLine");
        patternOrder.add("ThreadStartLine");
        patternOrder.add("ThreadEndLine");
    }

    public LogParser(String filename) throws FileNotFoundException {
        try {
            reader = new BufferedReader(new FileReader(new File(filename)));
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Couldn't create parser");
        }
    }

    public boolean hasNext() {
        try {
            return (currentLine = reader.readLine()) != null;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public LogLine next() throws EOFException {
        List<String> fieldVars = new ArrayList<String>();
        if (hasNext()) {
            for (String key : patternOrder) {
                Matcher m = allPatterns.get(key).matcher(currentLine);
                if (m.matches()) {
                    for (int i = 1; i <= m.groupCount(); i++) {
                        fieldVars.add(m.group(i));
                    }
                 /*   switch (key) {
                        case "CallLine": return new CallLine(fieldVars);
                        case "InstructionLine": return new InstructionLine(fieldVars);
                        case "MemoryReadLine": return new MemoryReadLine(fieldVars);
                        case "MemoryWriteLine": return new MemoryWriteLine(fieldVars);
                        case "ThreadStartLine": return new ThreadStartLine(fieldVars);
                        case "ThreadEndLine": return new ThreadEndLine(fieldVars);
                    }*/
                    if(key.compareTo("CallLine")== 0) return new CallLine(fieldVars);
                    if(key.compareTo("InstructionLine") == 0) return new InstructionLine(fieldVars);
                    if(key.compareTo("MemoryReadLine")==0) return new MemoryReadLine(fieldVars);
                    if(key.compareTo("MemoryWriteLine")==0) return new MemoryWriteLine(fieldVars);
                    if(key.compareTo( "ThreadStartLine")==0) return new ThreadStartLine(fieldVars);
                    if(key.compareTo("ThreadEndLine")== 0) return new ThreadEndLine(fieldVars);
                    
                    
                }
            }
            return new LogLine();
        } else {
            throw new EOFException("Ran out of instructions of IOException in hasNext");
        }
    }

    public void close() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}