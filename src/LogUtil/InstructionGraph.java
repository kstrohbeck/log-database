package LogUtil;


import java.io.EOFException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;


import com.orientechnologies.orient.client.remote.OServerAdmin;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.TransactionalGraph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

public class InstructionGraph {
	
	public static void main(String[] args)
	{ 
		InstructionGraph test = new InstructionGraph();
		TransactionalGraph db = test.GetDbHandle();
		
		try{
		if(db != null)
           test.insertData(db);
		
		if(db != null)
			test.retrieveData(db);
		if(db != null)
			test.retrieveAllData(db);
		

            if( db != null )
                db.shutdown();
            
		}catch(Exception e){}

		
		
	}
	public boolean createDb(String dbname)
	{
		try {
			
			
			OServerAdmin test = new OServerAdmin("remote:localhost");
		      test.connect("root", "root");
		      boolean status = test.existsDatabase(dbname);
		     if(status ==  false)
		      test.createDatabase(dbname,"graph","local").close();
		     
		      
		      return true;
			}catch(Exception ex){ return false;}
	}
	public TransactionalGraph GetDbHandle()
	{
		

		TransactionalGraph graph = null;

        try {
        	if(createDb("realinstructdb") == false)
        	{
        		//System.out.println("Db creation Failed");
        	}
        	
        	
            graph = new OrientGraph("remote:localhost/realinstructdb", "root", "root");
            System.out.println("Success");
            return graph;
            
            
            
            
            
            

        } catch (Exception e) {
  
            System.out.println("Not succeed - " + e.getMessage());
            return null;
  
        } 
        
	}
	
	public void insertData(TransactionalGraph graph) throws FileNotFoundException, EOFException
	{
		ArrayList<LogLine> sample = new ArrayList<LogLine>();
		
		 LogParser lp = new LogParser("/home/gbaduz/complex_sample.out");
	        while (lp.hasNext()) {
	        	sample.add(lp.next());
	            //System.out.println(lp.next().getFields());
	        }
	        lp.close();
		
		
		
		for(int j=0; j<sample.size();j=j+2){
			
			LogLine temp1 = sample.get(j);
			LogLine temp2 = sample.get(j+1);
			
			Vertex vInstruction = graph.addVertex("class:Instruction");
			for(Map.Entry<String,String> entry : temp1.fields.entrySet()){
				
				
				vInstruction.setProperty(entry.getKey(), entry.getValue());
				
				
			}
			
			Vertex vInstruction1 = graph.addVertex("class:Instruction");
			for(Map.Entry<String,String> entry : temp2.fields.entrySet()){
				
				
				vInstruction.setProperty(entry.getKey(), entry.getValue());
				
				
			} 	
		Edge enext = graph.addEdge(null, vInstruction, vInstruction1, "nextInst");
		
		
		}
		
	
	}
	public void retrieveData(TransactionalGraph graph)
	{
		Vertex temp = null;;
		for (Vertex vertex : graph.getVertices()) {
		    System.out.println(vertex.getProperty("address") + "| " + vertex.getProperty("name"));
		    temp= vertex;
		    break;
		  }
		  System.out.println("Edges of " + graph);
		  for (Edge edge : graph.getEdges()) {
		    System.out.println(edge);
		   }
		  Vertex a = graph.getVertex(temp);
		  System.out.println("vertex " + a.getId() + " has opcode " + a.getProperty("name"));
		  for(Edge e : a.getEdges(Direction.OUT)) {
		    System.out.println(e.getLabel());
		  }
	}
	public void retrieveAllData(TransactionalGraph graph)
	{
		
		for (Vertex vertex : graph.getVertices()) {
		    System.out.println(vertex.getProperty("address") + "| " + vertex.getProperty("name"));
		    
		  }
		  System.out.println("Edges of " + graph);
		  for (Edge edge : graph.getEdges()) {
		    System.out.println(edge);
		   }
		 
	}

}
