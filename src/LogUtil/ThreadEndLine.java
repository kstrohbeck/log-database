package LogUtil;
import java.util.Map;
import java.util.List;

public class ThreadEndLine extends LogLine {
    public ThreadEndLine(String tid, String time) {
    	fields.put("name", "threadendline");
        fields.put("tid", tid);
        fields.put("time", time);
    }

    public ThreadEndLine(List<String> fieldVars) {
        this(fieldVars.get(0), fieldVars.get(1));
    }
}